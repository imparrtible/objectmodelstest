import User from '../mongo/userShema';


// new User({id:1, acessLevel: [1,2]}).save();
// new User({id:10, acessLevel: [0]}).save();

const loadUserRights = async (userId) => {
    const user = await User.findOne({id: userId});
    return user.accessLevel;
};


const visitorRightsInExcessOf = (accessLevel) =>
    async (userId) =>{
        const userRights = await loadUserRights(userId);
        return (userRights.filter(right => right >= accessLevel).length > 0)
    };



export class UserModelObject {

    constructor(data) {
            this.id = data.id ? data.id : 'id is empty' ;
            this.AccessLevel = data.accessLevel;
    }


    static async gen(queryData, visitor) {
        const {targetId} = queryData;
        const {visitorId} = visitor;

        const data = await User.findOne({id: targetId});

        const actionAllowed = visitorRightsInExcessOf(0)(visitorId);

        if (data === null || data === undefined) return null;

        return actionAllowed ? new UserModelObject(data) : null

    }

    static async rem(queryData, visitor) {
        const {targetId} = queryData;
        const {visitorId} = visitor;

        const actionAllowed = await visitorRightsInExcessOf(1)(visitorId);

        if (! await User.findOne({id: targetId})) throw new Error('user doesnt exist') ;

        if (actionAllowed) {
            User.remove({id: targetId});
            return true
        }
        throw new Error('you have no permissions to remove users');
    }

    static async add(queryData, visitor) {
        const {targetId} = queryData;
        const {visitorId} = visitor;

        const actionAllowed = await visitorRightsInExcessOf(1)(visitorId);
        if (await User.findOne({id: targetId})) return 'user exists';

        if (actionAllowed) {
            new User({id: targetId}).save();
            return true
        }
        throw new Error('you have no permissions to add users');
    }
}