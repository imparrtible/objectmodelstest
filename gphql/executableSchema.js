import {
	GraphQLObjectType,
	GraphQLSchema,
} from 'graphql';

import mutations from './mutationsResolve'
import queries from './queriesResolve';

const queryList = {
	...queries
};
const mutationsList = {
	...mutations
};

export const Schema = new GraphQLSchema({
	query: new GraphQLObjectType({
		name: 'Query',
		fields: queryList,
	}),
	mutation: new GraphQLObjectType({
		name: 'Mutation',
		fields: mutationsList,
	}),
});