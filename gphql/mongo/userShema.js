import  mongoose from 'mongoose';

mongoose.Promise = global.Promise;

const adminUserSchema = new mongoose.Schema({
	accessLevel: {
		type: Array,
		default: [0]
	},
	name: {
		type: String
	},
	lastName: {
		type: String
	},
	id : {
		type: String
	}
});

export default mongoose.model('AdminUser', adminUserSchema)
