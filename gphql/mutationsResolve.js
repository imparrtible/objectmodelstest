import {
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLString
} from 'graphql';


import { UserModelObject} from './dataModels/userModelObject';


const userAdd = {
	/**
	 * возвращает true в случае успеха
	 */
	type: GraphQLBoolean,
	args: {
		targetId: {
		    name: 'targetId',
            type: new GraphQLNonNull(GraphQLString),
        }
	},
	async resolve(root, args, options) {
		return UserModelObject.add(args, options)
	}
};

const userRemove = {
	type: GraphQLBoolean,
	args: {
		targetId: {
		    name: 'targetId',
            type: new GraphQLNonNull(GraphQLString)
        }
	},
	async resolve(root, args, options){	
		return UserModelObject.rem(args, options)
	}
};

export default {
	userAdd,
	userRemove
};