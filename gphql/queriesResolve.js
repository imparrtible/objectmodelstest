import {
	GraphQLNonNull,
    GraphQLObjectType,
    GraphQLList,
	GraphQLInt,
    GraphQLString
} from 'graphql';

import {UserModelObject} from './dataModels/userModelObject';

const loadUser = {
	type: new GraphQLObjectType({
        name: 'User',
        fields: {
            id: {
                name: 'id',
                type: GraphQLString
            },
            AccessLevel: {
                name : 'AccessLevel',
                type : new GraphQLList(GraphQLInt)
            }
        }
    }),
	args: {
		targetId: {
            name: 'targetId',
            type: new GraphQLNonNull(GraphQLString),
        }
	},
	async resolve(root, args, options) {
        return UserModelObject.gen(args, options.context)
	}
};

export default {
	loadUser,
}