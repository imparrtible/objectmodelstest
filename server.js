import Koa from 'koa';
import KoaRouter from 'koa-router';
import cors from 'kcors';
import koaBody from 'koa-bodyparser';
import { graphqlKoa, graphiqlKoa } from 'graphql-server-koa';

import mongoose from 'mongoose';

import {Schema} from './gphql/executableSchema';

const URL = '172.17.0.2:27017/test';


const server = new Koa();
const router = new KoaRouter();


server.use(cors());
server.use(koaBody());


router.get('/graphql', graphqlKoa((ctx) => {
    return {
        schema: Schema,
        /**
         * здесь будет верифицироваться jwt токен,
         * после этого к примеру ID из него бросаем в context
         */
        context: { visitorId: 1 }
    };
}));

router.post('/graphql', graphqlKoa((ctx) => {
    return {
        schema: Schema,
        context: { visitorId: 1 }
    };
}));

router.get('/graphiql', graphiqlKoa({ endpointURL: '/graphql' }));

server.use(router.routes());
server.use(router.allowedMethods());

mongoose.connect(URL, (err) => {
    if (err) process.stdout.write(err);
    process.stdout.write('db connected');
});

server.listen(3000);